# PRUEBA TÉCNICA DE EDIMCA
## El proyecto esta desarrollo en microservicios

1) Backend: Java/Openjdk 11
2) Frontend: Reactjs
3) Base de datos: Postgresql 11
4) Contenedores: Docker
5) Servidor de aplicaciones embebido: apache tomcat (spring)
6) Seguridad: jwt
7) Framework backend: spring
8) Gestor de dependencias backend: maven
9) Framework frontend: React Js/Primereact/MUI
10) Gestor de dependencias frontend: npm

## Despliegue del proyecto

1) Clonar el proyecto
2) Ejecutar el comando: docker-compose build
3) Ejecutar el comando: docker-compose up

## Verificación

1) Para realizar una solicitud como empleado (employee) se debe abrir el link: http://localhost:3005/solicitar-registro
2) Para ingresar como director (manager) se debe ir al link: http://localhost:3005/login
3) Para logear al sistema se debe ingresar con el username: Administrador y Contraseña: administrador
4) El aplicativo tendra 4 pantallas donde se podrá Aprobar Solicitudes, Asignar Solicitudes, Historial de Solicitudes y Contador total de estados de las solicitudes.
