import { BrowserRouter as Router, Route, Routes, useNavigate } from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import SolicitarRegistro from './gob/ec/edimca/employee/SolicitarRegistro';
import ValidarSolicitudes from './gob/ec/edimca/manager/ValidarSolicitudes';
import AprobarSolicitudes from './gob/ec/edimca/manager/AprobarSolicitudes';
import AsignarSolicitudes from './gob/ec/edimca/manager/AsignarSolicitudes';
import HistorialSolicitudes from './gob/ec/edimca/manager/HistorialSolicitudes';
import ContadorDeSolicitudes from './gob/ec/edimca/manager/ContadorDeSolicitudes';
import Login from './gob/ec/edimca/login/Login';

function App() {
  return (
    <>
    <Router>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/solicitar-registro" element={<SolicitarRegistro />} />
          <Route path="/validar-solicitudes" element={<ValidarSolicitudes />} />
          <Route path="/aprobar-solicitudes" element={<AprobarSolicitudes />} />
          <Route path="/asignar-solicitudes" element={<AsignarSolicitudes />} />
          <Route path="/historial-solicitudes" element={<HistorialSolicitudes />} />
          <Route path="/contador-solicitudes" element={<ContadorDeSolicitudes />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
