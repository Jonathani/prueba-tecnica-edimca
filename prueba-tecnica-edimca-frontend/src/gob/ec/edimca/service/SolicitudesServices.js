import ServicioWeb from "../utilidades/ServicioWeb";
import Variables from "../utilidades/Variables";

class SolicitudesServices {

    static async getData(tipoSolicitudId) {
        let tokenApi = await ServicioWeb.obtenerToken(Variables.PETICION_USERNAME, Variables.PETICION_PASSWORD, Variables.PETICION_BACKEND_TOKEN);
        let rowDetallado = {
            tipoSolicitudId: tipoSolicitudId
        }
        let requestBody = {
            method: 'POST',
            headers: {
                Accept: "application/json",
                'Content-Type': 'application/json',
                'origin': 'x-request-with',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                Authorization: tokenApi
            },
            body: JSON.stringify(rowDetallado)
        }
        let response = await ServicioWeb.obtenerDatosConBody(
            Variables.PETICION_BACKEND_LISTAR_SOLICITUDES_POR_ID,
            requestBody
        );
        return response.status ? response.listaSolicitudes : [];

    }

    static async getSolicitudesPorIdMedium (tipoSolicitudId){
        return Promise.resolve(this.getData(tipoSolicitudId));
    }
}
export default SolicitudesServices;