import ServicioWeb from "../utilidades/ServicioWeb";
import Variables from "../utilidades/Variables";

class ContadorSolicitudesService {

    static async getData() {
        let tokenApi = await ServicioWeb.obtenerToken(Variables.PETICION_USERNAME, Variables.PETICION_PASSWORD, Variables.PETICION_BACKEND_TOKEN);
        let requestBody = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'origin': 'x-request-with',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                Authorization: tokenApi
            }
        };
        let response = await ServicioWeb.obtenerDatosConBody(
            Variables.PATICION_BACKEND_CONTADOR_DE_SOLICITUDES,
            requestBody
        );
        return response.status ? response.estadoSolcitudesResponseDto : [];
    }

    static async getContadorSolicitudesMedium() {
        return Promise.resolve(this.getData());
    }

}

export default ContadorSolicitudesService;