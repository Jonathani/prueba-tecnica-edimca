import ServicioWeb from "../utilidades/ServicioWeb";
import Variables from "../utilidades/Variables";

class HistorialSolicitudesService {

    static async getData() {
        let tokenApi = await ServicioWeb.obtenerToken(Variables.PETICION_USERNAME, Variables.PETICION_PASSWORD, Variables.PETICION_BACKEND_TOKEN);
        let requestBody = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'origin': 'x-request-with',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                Authorization: tokenApi
            }
        };
        let response = await ServicioWeb.obtenerDatosConBody(
            Variables.PATICION_BACKEND_LISTAR_HISTORIAL_SOLICITUDES,
            requestBody
        );
        return response.status ? response.historialSolicitudes : [];
    }

    static async getHistorialMedium() {
        return Promise.resolve(this.getData());
    }

}

export default HistorialSolicitudesService;