import React from "react";

class ServicioWeb extends React.Component {

    static async obtenerToken(user, password, url) {

        ///api/auth
        let requestOptions = {
            method: 'POST',
            headers: {
                Accept: "application/json",
                'Content-Type': 'application/json',
                'origin': 'x-request-with',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                'Access-Control-Allow-Credentials': 'true',
                'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS'
            },
            body: JSON.stringify({ username: user, password: password })
        }

        const response = await fetch(url, requestOptions);

        const data = await response.json();

        return data.jwttoken;
    }

    static async obtenerDatosConBody(Url, requestOptions) {
        const response = await fetch(Url, requestOptions);
        const data = await response.json();
        return data;
    }

}

export default ServicioWeb;
