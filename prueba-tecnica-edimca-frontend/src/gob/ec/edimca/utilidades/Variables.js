class Variables {

    // Credenciales api token
    static PETICION_USERNAME = "edimca";

    static PETICION_PASSWORD = "password";

    //Endpoint apis
    static PETICION_BACKEND_TOKEN = "http://10.0.172.3:8081/authenticate";

    static PETICION_BACKEND_SAVE_EMPLOYEE = "http://10.0.172.3:8081/api/saveEmployee";

    static PETICION_BACKEND_LISTAR_PRIORIDADES = "http://10.0.172.3:8081/api/listarPrioridades";

    static PETICION_BACKEND_LISTAR_SOLICITUDES_POR_ID = "http://10.0.172.3:8081/api/listarSolicitudes";

    static PETICION_BACKEND_ACTUALIZAR_SOLICITUD = "http://10.0.172.3:8081/api/actualizarEstado/";

    static PETICION_BACKEND_VALIDAR_LOGIN = "http://10.0.172.3:8081/api/login";

    static PATICION_BACKEND_LISTAR_INFORMACION_USUARIOS = "http://10.0.172.3:8081/api/informacionUsuario";

    static PATICION_BACKEND_LISTAR_HISTORIAL_SOLICITUDES = "http://10.0.172.3:8081/api/historialSolicitudes";

    static PATICION_BACKEND_CONTADOR_DE_SOLICITUDES = "http://10.0.172.3:8081/api/estadosSolicitudes";

    // Tiempo de toast
    static TIEMPO_DE_TOAST = 6000;

    // Ids de tipo de solicitudes
    static PENDING = 9;
    static APPROVED = 10;
    static ASSIGNED = 11;

    // Ids de tipo de solicitudes
    static PENDING_LABEL = 'PENDING';
    static APPROVED_LABEL = 'APPROVED';
    static ASSIGNED_LABEL = 'ASSIGNED';
    static PENDING_VALUE = 9;
    static APPROVED_VALUE = 10;
    static ASSIGNED_VALUE = 11;

} export default Variables;