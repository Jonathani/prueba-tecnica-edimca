class Mensajes {

    static ACEPTAR_GUARDADO = "¿Esta seguro que desea enviar una solicitud?";

    static ACTUALIZADO_CON_EXITO = "¿Esta seguro que desea actualizar la informacion de la solicitud?"

    static SELECCIONAR_ESTADO_SOLICITUD = "Estimado Usuario, debe seleccionar el estado nuevo de la solicitud, para continuar"

} export default Mensajes;