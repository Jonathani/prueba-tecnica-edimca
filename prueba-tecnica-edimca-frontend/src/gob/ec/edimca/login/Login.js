import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primeflex/primeflex.css";
import React, { useState, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { InputText } from 'primereact/inputtext';
import { Password } from 'primereact/password';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
import { Dialog } from 'primereact/dialog';
import { classNames } from 'primereact/utils';
import { Controller, useForm } from 'react-hook-form';
import Variables from '../utilidades/Variables';
import Mensajes from '../utilidades/Mensajes';
import ServicioWeb from '../utilidades/ServicioWeb';

import './Login.css'

function Login() {

    const navigate = useNavigate();
    const toast = useRef(null);
    const defaultValues = { username: '', contrasenia: '' };
    const form = useForm({ defaultValues });
    const errors = form.formState.errors;

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> : <small className="p-error">&nbsp;</small>;
    };

    const onSubmit = (data) => {
        let username = data.username.trim();
        let password = data.contrasenia.trim();
        ; (async () => {
            let tokenApi = await ServicioWeb.obtenerToken(Variables.PETICION_USERNAME, Variables.PETICION_PASSWORD, Variables.PETICION_BACKEND_TOKEN);
            let rowDetallado = {
                username: username,
                password: password
            }
            let requestBody = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let response = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_VALIDAR_LOGIN,
                requestBody
            );
            if (response.status){
                let rowDetalladoUsuario = {
                    tipoUsuarioId: "0",
                    nombreUsuario: username
                }
                let requestBodyUsuario = {
                    method: 'POST',
                    headers: {
                        Accept: "application/json",
                        'Content-Type': 'application/json',
                        'origin': 'x-request-with',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                        'Access-Control-Allow-Credentials': 'true',
                        'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                        Authorization: tokenApi
                    },
                    body: JSON.stringify(rowDetalladoUsuario)
                }
                let responseUsuario = await ServicioWeb.obtenerDatosConBody(
                    Variables.PATICION_BACKEND_LISTAR_INFORMACION_USUARIOS,
                    requestBodyUsuario
                );
                localStorage.setItem('infoUsuarioSesion', JSON.stringify(responseUsuario.usuarios[0]));
                showToast('success', 'Exitoso', response.mensaje, Variables.TIEMPO_DE_TOAST);
                navigate("/validar-solicitudes", { replace: true });                
            }else{
                showToast('error', 'Error', response.mensaje, Variables.TIEMPO_DE_TOAST);
            }
        })();
    }

    return (
        <>
            <Toast ref={toast} />
            <div className="loginCSS">
                <div className="surface-card p-4 shadow-2 border-round">
                    <div className="text-center mb-5">
                        <div className="text-900 text-3xl font-medium mb-2">Login</div>
                    </div>

                    <div>
                        <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-column gap-2">
                            <Controller
                                name="username"
                                control={form.control}
                                rules={{ required: 'El username es obligatorio.' }}
                                render={({ field, fieldState }) => (
                                    <>
                                        <label htmlFor={field.name}>Username</label>
                                        <InputText id={field.name} {...field} type="text" maxLength={20} placeholder="Username" className={classNames({ 'p-invalid': fieldState.error })} />
                                        {getFormErrorMessage(field.name)}
                                    </>
                                )}
                            />
                            <Controller
                                name="contrasenia"
                                control={form.control}
                                rules={{ required: 'La contraseña es obligatoria.' }}
                                render={({ field, fieldState }) => (
                                    <>
                                        <label htmlFor={field.name}>Contraseña</label>
                                        <Password id={field.name} {...field} type="password" maxLength={60} placeholder="Contraseña" className={classNames({ 'p-invalid': fieldState.error })} feedback={false} toggleMask />
                                        {getFormErrorMessage(field.name)}
                                    </>
                                )}
                            />
                            <Button label="Ingresar" icon="pi pi-user" type="submit" className="w-full" />
                        </form>
                    </div>
                </div>
            </div>

        </>
    );

}

export default Login
