import React, { useState, useEffect, useRef } from 'react'
import { styled, useTheme } from '@mui/material/styles';
import { Toast } from 'primereact/toast';
import { Panel } from 'primereact/panel';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';
import { ConfirmDialog } from 'primereact/confirmdialog';
import SolicitudesServices from '../service/SolicitudesServices';
import Variables from '../utilidades/Variables';
import Mensajes from '../utilidades/Mensajes';
import ServicioWeb from '../utilidades/ServicioWeb';
import ContadorSolicitudesService from '../service/ContadorSolicitudesService';

import './AsignarSolicitudes.css';

function AsignarSolicitudes() {

    const dt = useRef(null);
    const toast = useRef(null);
    const [solicitudesPendientes, setSolicitudesPendientes] = useState([]);
    const [loading, setLoading] = useState(true);
    const [saving, setSaving] = useState(false);
    const [usuarioActualizar, setUsuarioActualizar] = useState(false);
    const [visibleActualizar, setVisibleActualizar] = useState(false);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [estadoSolicitud, setEstadoSolicitud] = useState();
    const [usuarioId, setUsuarioId] = useState();
    const [contadorDeSolicitudes, setContadorDeSolicitudes] = useState([]);
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        solicitudId: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        descripcion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        prioridad: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    //Informacion Usuario
    const [nombre, setNombre] = useState('');
    const [descripcion, setDescripcion] = useState('');
    const [prioridad, setPrioridad] = useState('');

    //Estado Solicitud
    const [estadoSolicitudId, setEstadoSolicitudId] = useState(null);
    const estadosSolicitudes = [
        { name: Variables.ASSIGNED_LABEL, code: Variables.ASSIGNED_VALUE },
    ];

    const [usuariosAnalistas, setUsuariosAnalistas] = useState([]);
    const [usuarioAnalistaId, setUsuarioAnalistaId] = useState();

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const getSolicitudesPendientes = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const getContadorDeSolicitudes = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const cargarDatosData = () => {
        SolicitudesServices.getSolicitudesPorIdMedium(Variables.APPROVED).then((data) => {
            setSolicitudesPendientes(getSolicitudesPendientes(data))
            setLoading(false)
        });
    }

    const cargarDatosContador = () => {
        ContadorSolicitudesService.getContadorSolicitudesMedium().then((data) => {
            const listItems = getContadorDeSolicitudes(data).map((item) =>
                <>
                    <b> {item.estadoSolicitud} </b> : {item.total}
                </> 
            );
            setContadorDeSolicitudes(listItems)
            setLoading(false)
        });
    }

    useEffect(() => {
        cargarDatosData();
        cargarDatosContador();
    }, []);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'solicitudId', header: 'Solicitud Id' },
        { field: 'nombre', header: 'Nombre' },
        { field: 'descripcion', header: 'Descripcion' },
        { field: 'prioridad', header: 'Prioridad' }
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, solicitudesPendientes);
                doc.save('solicitudesPendientes.pdf');
            });
        });
    };

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };


    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(solicitudesPendientes);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'solicitudesPendientes');
        });
    };


    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{ fontSize: '10px' }} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <Button style={{ fontSize: '10px' }} type="button" icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button style={{ fontSize: '10px' }} type="button" icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button style={{ fontSize: '10px' }} type="button" icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    const mostrarRespuestaHistorial = (rowData) => {
        setNombre(rowData.nombre)
        setDescripcion(rowData.descripcion)
        setPrioridad(rowData.prioridad)
        setUsuarioId(rowData.solicitudId)
        setUsuarioActualizar(true)
        setEstadoSolicitud(rowData)
        ; (async () => {
            let tokenApi = await ServicioWeb.obtenerToken(Variables.PETICION_USERNAME, Variables.PETICION_PASSWORD, Variables.PETICION_BACKEND_TOKEN);
            let rowDetallado = {
                tipoUsuarioId: "12",
                nombreUsuario: "0"
            }
            let requestBody = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let response = await ServicioWeb.obtenerDatosConBody(
                Variables.PATICION_BACKEND_LISTAR_INFORMACION_USUARIOS,
                requestBody
            );
            if (response.status){
                setUsuariosAnalistas(response.usuarios);
            }
        })();
    }

    const visualizarRespuesta = (rowData) => {
        return (
            <React.Fragment>
                <Button icon="pi pi-table" style={{ fontSize: '10px' }} rounded outlined className="mr-2" label="Actualizar Estado" onClick={() => mostrarRespuestaHistorial(rowData)} />
            </React.Fragment>
        );
    }

    const hideDialogEditar = () => {
        setUsuarioActualizar(false)
    }

    const aceptarDatosGuardado = () => {
        if (estadoSolicitudId != null && usuarioAnalistaId != null){
            setVisibleActualizar(true)
        }else{
            showToast('error', 'Error', Mensajes.SELECCIONAR_ESTADO_SOLICITUD, Variables.TIEMPO_DE_TOAST);
        }        
    }

    const guardarDatosActualizados = () => {

        ; (async () => {
            let tokenApi = await ServicioWeb.obtenerToken(Variables.PETICION_USERNAME, Variables.PETICION_PASSWORD, Variables.PETICION_BACKEND_TOKEN);
            let rowDetallado = {
                estadoSolicitudId: estadoSolicitudId.code,
                usuarioAsignadoId: usuarioAnalistaId.usuarioId
            }
            let requestBody = {
                method: 'PUT',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let response = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_ACTUALIZAR_SOLICITUD + usuarioId,
                requestBody
            );
            if (response.status) {
                showToast('success', 'Exitoso', response.mensaje, Variables.TIEMPO_DE_TOAST);
                setUsuarioActualizar(false)
                cargarDatosData();
            } else {
                showToast('error', 'Error', response.mensaje, Variables.TIEMPO_DE_TOAST);
            }
        })();
    }

    const usuarioDialogFooterNuevo = (
        <>
            <Toast ref={toast} />
            <ConfirmDialog visible={visibleActualizar} onHide={() => setVisibleActualizar(false)} message={Mensajes.ACTUALIZADO_CON_EXITO}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={guardarDatosActualizados} />
            <React.Fragment>
                <Button label="Cancelar" style={{ fontSize: '10px' }} icon="pi pi-times" outlined onClick={hideDialogEditar} />
                <Button label="Guardar" style={{ fontSize: '10px' }} loading={saving} icon="pi pi-check" onClick={aceptarDatosGuardado} />
            </React.Fragment>
        </>
    );

    return (
        <div id= "asignarDiv">
            <Toast ref={toast} />
            <DrawerHeader />
            <div className="container_validar_solicitudes">
                <Panel header={<h5>Solicitudes por Asignar</h5>}>
                    <>{contadorDeSolicitudes}</>
                    <DataTable value={solicitudesPendientes}
                        paginator
                        rows={10}
                        dataKey="idUsuario"
                        rowsPerPageOptions={[10, 25, 50]}
                        ref={dt}
                        filters={filters}
                        scrollable="true"
                        filterDisplay="row"
                        loading={loading}
                        globalFilterFields={['solicitudId', 'nombre', 'descripcion', 'prioridad']}
                        header={header}
                        action={<AsignarSolicitudes loadParameter={[cargarDatosData, cargarDatosContador]}  />}
                        emptyMessage="Solicitudes no encontrados"
                        style={{ fontSize: '10px' }}
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries">

                        <Column field="solicitudId" sortable header="Solicitud Id" footer="Solicitud Id" filter filterPlaceholder="Buscar por Solicitud Id" style={{ fontSize: '10px' }} />
                        <Column field="nombre" sortable header="Nombre" footer="Nombre" filter filterPlaceholder="Buscar por Nombres" style={{ fontSize: '10px' }} />
                        <Column field="descripcion" sortable header="Descripcion" footer="Descripcion" filter filterPlaceholder="Buscar por Descripcion" style={{ fontSize: '10px' }} />
                        <Column field="prioridad" sortable header="Prioridad" footer="Prioridad" filter filterPlaceholder="Buscar por Prioridad" style={{ fontSize: '10px' }} />
                        <Column body={visualizarRespuesta} exportable={false} style={{ fontSize: '10px' }}></Column>
                    </DataTable>
                    {/* Dialogo para Crear nuevo usuario */}
                    <Dialog visible={usuarioActualizar} style={{ width: '32rem' }}
                        breakpoints={{ '960px': '75vw', '641px': '90vw' }}
                        header="Actualizar Estado" modal className="p-fluid"
                        footer={usuarioDialogFooterNuevo}
                        onHide={hideDialogEditar}>
                        <div className="field">
                            <label htmlFor="nombre" className="font-bold" style={{ fontSize: '10px' }}>
                                Nombre
                            </label>
                            <InputText id="nombre" type="text" style={{ fontSize: '10px' }} defaultValue={nombre}  value={nombre} />
                        </div>
                        <div className="field">
                            <label htmlFor="descripcion" className="font-bold" style={{ fontSize: '10px' }}>
                                Descripcion
                            </label>
                            <InputText id="descripcion" type="text" style={{ fontSize: '10px' }} defaultValue={descripcion}  value={descripcion} />
                        </div>
                        <div className="field">
                            <label htmlFor="prioridad" className="font-bold" style={{ fontSize: '10px' }}>
                                Prioridad
                            </label>
                            <InputText id="prioridad" type="text" style={{ fontSize: '10px' }} defaultValue={prioridad}  value={prioridad} />
                        </div>
                        <div className="field">
                            <label htmlFor="usuarioAsignar" className="font-bold" style={{ fontSize: '10px' }}>
                                Usuario a asignar
                            </label>
                            <Dropdown value={usuarioAnalistaId} onChange={(e) => setUsuarioAnalistaId(e.value)} options={usuariosAnalistas} optionLabel="nombreUsuario" 
                                placeholder="Seleccione el Usuario" checkmark={true} highlightOnSelect={false} />
                        </div>
                        <div className="field">
                            <label htmlFor="estadoSolicitud" className="font-bold" style={{ fontSize: '10px' }}>
                                Estado Solicitud
                            </label>
                            <Dropdown value={estadoSolicitudId} onChange={(e) => setEstadoSolicitudId(e.value)} options={estadosSolicitudes} optionLabel="name" 
                                placeholder="Seleccione el Estado" checkmark={true} highlightOnSelect={false} />
                        </div>
                    </Dialog>
                </Panel>
            </div>
        </div>
    );
}

export default AsignarSolicitudes