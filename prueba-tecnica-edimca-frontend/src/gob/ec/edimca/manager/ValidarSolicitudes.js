import React, { useState, useEffect, useRef } from 'react'
import { styled, useTheme } from '@mui/material/styles';
import ReactDOM, { createRoot } from 'react-dom/client';
import { Toast } from 'primereact/toast';
import * as Icons from "react-icons/fa";
import {
    useNavigate,
    Routes,
    Route,
    BrowserRouter as Router
} from "react-router-dom";
import { Panel } from 'primereact/panel';
import MuiAppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ExitToApp from "@mui/icons-material/ExitToApp";
import { Button } from 'primereact/button';
import AprobarSolicitudes from './AprobarSolicitudes';
import AsignarSolicitudes from './AsignarSolicitudes';
import HistorialSolicitudes from './HistorialSolicitudes';
import ContadorDeSolicitudes from './ContadorDeSolicitudes';

import './ValidarSolicitudes.css'

const drawerWidth = 250;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        flexGrow: 1,
        padding: theme.spacing(3),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: `-${drawerWidth}px`,
        ...(open && {
            transition: theme.transitions.create('margin', {
                easing: theme.transitions.easing.easeOut,
                duration: theme.transitions.duration.enteringScreen,
            }),
            marginLeft: 0,
        }),
    }),
);

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: `${drawerWidth}px`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
}));


function ValidarSolicitudes() {

    const theme = useTheme();
    const toast = useRef(null);
    const [open, setOpen] = React.useState(false);
    const ref = useRef(null);

    const navigate = useNavigate();
    const informacionSesion = localStorage.getItem('infoUsuarioSesion');
    const objetoSesion = JSON.parse(informacionSesion);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const salirApp = () => {
        window.localStorage.clear();
        navigate("/login");
    }

    const aprobarSolicitudes = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <AprobarSolicitudes />
                </Router>
            </React.StrictMode>
        );
    }

    const asignarSolicitudes = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <AsignarSolicitudes />
                </Router>
            </React.StrictMode>
        );
    }

    const historialSolicitudes = () => {
        const root = ReactDOM.createRoot(document.getElementById("idContenido"));
        root.render(
            <React.StrictMode>
                <Router>
                    <HistorialSolicitudes />
                </Router>
            </React.StrictMode>
        );
    }

    const contadorDeSolicitudes = () => {
        var myCoolDiv = document.getElementById("idContenido");
        const root = ReactDOM.createRoot(myCoolDiv);
        root.render(
            <React.StrictMode>
                <Router>
                    <ContadorDeSolicitudes />
                </Router>
            </React.StrictMode>
        );
    }

    return (
        informacionSesion !== null ? (
            <>
                <Toast ref={toast} />
                <Box sx={{ display: 'flex' }}>
                    <CssBaseline />
                    <AppBar position="fixed" open={open}>
                        <Toolbar>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                onClick={handleDrawerOpen}
                                edge="start"
                                sx={{ mr: 2, ...(open && { display: 'none' }) }}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography variant="h10" noWrap component="div" style={{ fontSize: '12px' }}>
                                Menú Principal
                            </Typography>
                            <Typography variant="h10" noWrap component="div" style={{ width: '80%', textAlign: 'right', fontSize: '12px' }}>
                                Nombre: {objetoSesion.nombreUsuario}
                            </Typography>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Button label="Salir" icon="pi pi-sign-out" iconPos="right" style={{ width: '8%', 'backgroundColor': '#008CBA', fontSize: '12px' }} onClick={salirApp} />
                        </Toolbar>
                    </AppBar>
                    <Drawer
                        sx={{
                            width: drawerWidth,
                            flexShrink: 0,
                            '& .MuiDrawer-paper': {
                                width: drawerWidth,
                                boxSizing: 'border-box',
                            },
                        }}
                        variant="persistent"
                        anchor="left"
                        open={open}
                    >
                        <DrawerHeader>
                            <IconButton onClick={handleDrawerClose}>
                                {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                            </IconButton>
                        </DrawerHeader>
                        <Divider />
                        <List>
                            <ListItem key={'Solicitudes Pendientes'} disablePadding>
                                <ListItemButton sx={{ pl: 4 }} onClick={aprobarSolicitudes}>
                                    <ListItemIcon>
                                        <ExitToApp />
                                    </ListItemIcon>
                                    <ListItemText
                                        primaryTypographyProps={{
                                            fontSize: 12
                                        }}
                                        primary={'Solicitudes Pendientes'}
                                    />
                                </ListItemButton>
                            </ListItem>
                            <ListItem key={'Solicitudes Aprobadas'} disablePadding>
                                <ListItemButton sx={{ pl: 4 }} onClick={asignarSolicitudes}>
                                    <ListItemIcon>
                                        <ExitToApp />
                                    </ListItemIcon>
                                    <ListItemText
                                        primaryTypographyProps={{
                                            fontSize: 12
                                        }}
                                        primary={'Solicitudes Aprobadas'}
                                    />
                                </ListItemButton>
                            </ListItem>
                            <ListItem key={'Historial de Solicitudes'} disablePadding>
                                <ListItemButton sx={{ pl: 4 }} onClick={historialSolicitudes}>
                                    <ListItemIcon>
                                        <ExitToApp />
                                    </ListItemIcon>
                                    <ListItemText
                                        primaryTypographyProps={{
                                            fontSize: 12
                                        }}
                                        primary={'Historial de Solicitudes'}
                                    />
                                </ListItemButton>
                            </ListItem>
                            <ListItem key={'Contador estado de solicitudes'} disablePadding>
                                <ListItemButton sx={{ pl: 4 }} onClick={contadorDeSolicitudes}>
                                    <ListItemIcon>
                                        <ExitToApp />
                                    </ListItemIcon>
                                    <ListItemText
                                        primaryTypographyProps={{
                                            fontSize: 12
                                        }}
                                        primary={'Contador estado de solicitudes'}
                                    />
                                </ListItemButton>
                            </ListItem>

                        </List>
                    </Drawer>
                    <Main id='idContenido' open={open}>
                    
                    </Main>
                </Box>
            </>
        ) : (
            <>
                <div className='app_noautorizado_menus'>
                    <br></br>
                    <br></br>
                    <Panel ref={ref} header="Acceso No Autorizado">
                        <Button onClick={salirApp}>Regresar Login</Button>
                    </Panel>
                </div>
            </>
        )
    );
}

export default ValidarSolicitudes