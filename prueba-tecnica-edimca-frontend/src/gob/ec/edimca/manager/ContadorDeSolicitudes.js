import React, { useState, useEffect, useRef } from 'react'
import { styled, useTheme } from '@mui/material/styles';
import { Toast } from 'primereact/toast';
import { Panel } from 'primereact/panel';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { FilterMatchMode } from 'primereact/api';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import ContadorSolicitudesService from '../service/ContadorSolicitudesService';

import './ContadorDeSolicitudes.css';

function ContadorDeSolicitudes() {

    const dt = useRef(null);
    const toast = useRef(null);
    const [contadorDeSolicitudes, setContadorDeSolicitudes] = useState([]);
    const [loading, setLoading] = useState(true);
    const [globalFilterValue, setGlobalFilterValue] = useState('');
    const [filters, setFilters] = useState({
        global: { value: null, matchMode: FilterMatchMode.CONTAINS },
        solicitudId: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        nombre: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        descripcion: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
        prioridad: { value: null, matchMode: FilterMatchMode.STARTS_WITH },
    });

    const DrawerHeader = styled('div')(({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    }));

    const getContadorDeSolicitudes = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const cargarDatosData = () => {
        ContadorSolicitudesService.getContadorSolicitudesMedium().then((data) => {
            setContadorDeSolicitudes(getContadorDeSolicitudes(data))
            setLoading(false)
        });
    }

    useEffect(() => {
        cargarDatosData();
    }, []);

    const onGlobalFilterChange = (e) => {
        const value = e.target.value;
        let _filters = { ...filters };
        _filters['global'].value = value;
        setFilters(_filters);
        setGlobalFilterValue(value);
    };

    const cols = [
        { field: 'estadoSolicitud', header: 'Estado Solicitud' },
        { field: 'total', header: 'Total' }
    ];

    const exportColumns = cols.map((col) => ({ title: col.header, dataKey: col.field }));

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    };

    const exportPdf = () => {
        import('jspdf').then((jsPDF) => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);

                doc.autoTable(exportColumns, contadorDeSolicitudes);
                doc.save('contadorDeSolicitudes.pdf');
            });
        });
    };

    const saveAsExcelFile = (buffer, fileName) => {
        import('file-saver').then((module) => {
            if (module && module.default) {
                let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                let EXCEL_EXTENSION = '.xlsx';
                const data = new Blob([buffer], {
                    type: EXCEL_TYPE
                });
                module.default.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
            }
        });
    };

    const exportExcel = () => {
        import('xlsx').then((xlsx) => {
            const worksheet = xlsx.utils.json_to_sheet(contadorDeSolicitudes);
            const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
            const excelBuffer = xlsx.write(workbook, {
                bookType: 'xlsx',
                type: 'array'
            });
            saveAsExcelFile(excelBuffer, 'contadorDeSolicitudes');
        });
    };

    const header = (
        <div className="flex justify-content-end">
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText style={{ fontSize: '10px' }} value={globalFilterValue} onChange={onGlobalFilterChange} placeholder="Buscar" />
            </span>
            <Button style={{ fontSize: '10px' }} type="button" icon="pi pi-file" rounded onClick={() => exportCSV(false)} data-pr-tooltip="CSV" label="CSV" />
            <Button style={{ fontSize: '10px' }} type="button" icon="pi pi-file-excel" severity="success" rounded onClick={exportExcel} data-pr-tooltip="XLS" label="XLSX" />
            <Button style={{ fontSize: '10px' }} type="button" icon="pi pi-file-pdf" severity="warning" rounded onClick={exportPdf} data-pr-tooltip="PDF" label="PDF" />
        </div>
    );

    return (
        <div id= "contadorDiv">
            <Toast ref={toast} />
            <DrawerHeader />
            <div className="container_validar_solicitudes">
                <Panel header={<h5>Contador estado de solicitudes</h5>}>
                    <DataTable value={contadorDeSolicitudes}
                        paginator
                        rows={10}
                        rowsPerPageOptions={[10, 25, 50]}
                        ref={dt}
                        filters={filters}
                        scrollable="true"
                        filterDisplay="row"
                        loading={loading}
                        globalFilterFields={['estadoSolicitud', 'total']}
                        header={header}
                        action={<ContadorDeSolicitudes loadParameter={cargarDatosData} />}
                        emptyMessage="Solicitudes no encontrados"
                        style={{ fontSize: '10px' }}
                        paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                        currentPageReportTemplate="Showing {first} to {last} of {totalRecords} entries">

                        <Column field="estadoSolicitud" sortable header="Estado Solicitud" footer="Estado Solicitud" filter filterPlaceholder="Buscar por Estado Solicitud" style={{ fontSize: '10px' }} />
                        <Column field="total" sortable header="Total" footer="Total" filter filterPlaceholder="Buscar por Total" style={{ fontSize: '10px' }} />
                    </DataTable>
                </Panel>
            </div>
        </div>
    );
}

export default ContadorDeSolicitudes