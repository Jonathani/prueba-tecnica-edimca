import React, { useState, useEffect, useRef } from 'react'
import { Toast } from 'primereact/toast';
import { Controller, useForm } from 'react-hook-form';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { ConfirmDialog } from 'primereact/confirmdialog';
import { classNames } from 'primereact/utils';
import PrioridadesService from '../service/PrioridadesService';
import ServicioWeb from '../utilidades/ServicioWeb';
import Variables from '../utilidades/Variables';
import Mensajes from '../utilidades/Mensajes';
import './SolicitarRegistro.css';

function SolicitarRegistro() {

    const toast = useRef(null);
    const defaultValues = { nombre: '', descripcion: '', prioridadId: '' };
    const form = useForm({ defaultValues });
    const errors = form.formState.errors;
    const [saving, setSaving] = useState(false);
    const [nombre, setNombre] = useState([]);
    const [descripcion, setDescripcion] = useState([]);
    const [prioridades, setPrioridades] = useState([]);
    const [selectedPrioridad, setSelectedPrioridad] = useState(null);

    const getPrioridades = (data) => {
        return [...(data || [])].map((d) => {
            d.date = new Date(d.date);
            return d;
        });
    };

    const cargarDatosData = () => {
        PrioridadesService.getPrioridadesMedium().then((data) => {
            setPrioridades(getPrioridades(data));
        });
    }

    useEffect(() => {
        cargarDatosData();
    }, []);

    const showToast = (severityValue, summaryValue, detailValue, timeLife) => {
        toast?.current?.show({ severity: severityValue, summary: summaryValue, detail: detailValue, life: timeLife });
    }

    const getFormErrorMessage = (name) => {
        return errors[name] ? <small className="p-error">{errors[name].message}</small> : <small className="p-error">&nbsp;</small>;
    };

    const guardarInformacion = () => {
        ; (async () => {
            let tokenApi = await ServicioWeb.obtenerToken(Variables.PETICION_USERNAME, Variables.PETICION_PASSWORD, Variables.PETICION_BACKEND_TOKEN);
            let rowDetallado = {
                nombre: nombre,
                descripcion: descripcion,
                prioridadId: selectedPrioridad.catalogoId
            }
            let requestBody = {
                method: 'POST',
                headers: {
                    Accept: "application/json",
                    'Content-Type': 'application/json',
                    'origin': 'x-request-with',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, X-Auth-Token',
                    'Access-Control-Allow-Credentials': 'true',
                    'Access-Control-Expose-Headers': 'Content-Length, X-Kuma-Revision',
                    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    Authorization: tokenApi
                },
                body: JSON.stringify(rowDetallado)
            }
            let response = await ServicioWeb.obtenerDatosConBody(
                Variables.PETICION_BACKEND_SAVE_EMPLOYEE,
                requestBody
            );
            if (response.estado) {
                showToast('success', 'Exitoso', response.respuesta, Variables.TIEMPO_DE_TOAST);
            } else {
                showToast('error', 'Error', response.mensaje, Variables.TIEMPO_DE_TOAST);
            }
        })();
    }

    const onSubmit = (data) => {
        setNombre(data.nombre);
        setDescripcion(data.descripcion);
        setSaving(true)
    }

    return (
        <>
            <Toast ref={toast} />
            <ConfirmDialog visible={saving} onHide={() => setSaving(false)} message={Mensajes.ACEPTAR_GUARDADO}
                header="Confirmación" icon="pi pi-exclamation-triangle" accept={guardarInformacion} />

            <div className="container_solicitar_registro">

                {/* <div className="surface-card p-3 shadow-2 border-round"> */}
                <div className="text-center mb-5">
                    <div className="text-900 text-3xl font-medium mb-2">SOLICITAR REGISTRO</div>
                </div>
                {/* </div> */}

                <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-column gap-1">
                    <div className="col-12 md:col-4 mb-4 px-6">

                        <Controller
                            name="nombre"
                            control={form.control}
                            rules={{ required: 'El nombre es obligatorio.' }}
                            render={({ field, fieldState }) => (
                                <>
                                    <label htmlFor={field.name}>Nombre: </label>
                                    <InputText id={field.name} {...field} type="text" maxLength={60} placeholder="nombre" className={classNames({ 'p-invalid': fieldState.error })} />
                                    {getFormErrorMessage(field.name)}
                                </>
                            )}
                        />
                        <br></br>
                        <Controller
                            name="descripcion"
                            control={form.control}
                            rules={{ required: 'La descripción es obligatorio.' }}
                            render={({ field, fieldState }) => (
                                <>
                                    <label htmlFor={field.name}>Descripción: </label>
                                    <InputText id={field.name} {...field} type="text" maxLength={60} placeholder="descripcion" className={classNames({ 'p-invalid': fieldState.error })} />
                                    {getFormErrorMessage(field.name)}
                                </>
                            )}
                        />
                        <br></br>
                        <Controller
                            name="descripcion"
                            control={form.control}
                            rules={{ required: 'La prioridad es obligatorio.' }}
                            render={({ field, fieldState }) => (
                                <>
                                    <label htmlFor={field.name}>Prioridad: </label>
                                    <Dropdown
                                        id={field.name} {...field}
                                        value={selectedPrioridad}
                                        onChange={(e) => setSelectedPrioridad(e.value)}
                                        options={prioridades}
                                        optionLabel="descripcion"
                                        placeholder="Seleccione la Prioridad"
                                        className={classNames({ 'p-invalid': fieldState.error })} />
                                    {getFormErrorMessage(field.name)}
                                </>
                            )}
                        />
                    </div>
                    <br></br>
                    <Button label="Registrar" icon="pi pi-user" loading={saving} type="submit" className="w-full" />
                </form>
            </div>
        </>
    );

}

export default SolicitarRegistro
