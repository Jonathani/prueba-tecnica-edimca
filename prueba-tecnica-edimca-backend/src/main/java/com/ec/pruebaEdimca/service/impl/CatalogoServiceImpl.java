package com.ec.pruebaEdimca.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ec.pruebaEdimca.dao.CatalogoDao;
import com.ec.pruebaEdimca.dto.CatalogoResponseDto;
import com.ec.pruebaEdimca.dto.ListarSolicitudesRequestDto;
import com.ec.pruebaEdimca.dto.ListarSolicitudesResponseDto;
import com.ec.pruebaEdimca.service.CatalogoService;

@Service
public class CatalogoServiceImpl implements CatalogoService{
	
	@Autowired
	CatalogoDao catalogoDao;

	@Override
	public List<CatalogoResponseDto> listarTipoPrioridad(long prioridadId) {
		
		List<CatalogoResponseDto> listaPrioridad = new ArrayList<>();
		
		catalogoDao.listarTipoPrioridad(prioridadId).forEach(
			(e) -> {
				listaPrioridad.add(
					new CatalogoResponseDto(String.valueOf(e.getCatalogoId()), e.getDescripcion())	
				);				
			}				
		);		
		return listaPrioridad;
	}

	@Override
	public List<ListarSolicitudesResponseDto> listarSolicitudesPorId(ListarSolicitudesRequestDto listarSolicitudesRequestDto) {
		List<ListarSolicitudesResponseDto> listaSolicitudes = new ArrayList<>();
		catalogoDao.consultarSolicitudesPorId(Long.parseLong(listarSolicitudesRequestDto.getTipoSolicitudId())).forEach(
			(e) -> {
				listaSolicitudes.add(
					new ListarSolicitudesResponseDto(
						e[0] != null ? e[0].toString() : "", 
						e[1] != null ? e[1].toString() : "", 
						e[2] != null ? e[2].toString() : "",
						e[3] != null ? e[3].toString() : ""
					)
				);
			}
		);
		// TODO Auto-generated method stub
		return listaSolicitudes;
	}

}
