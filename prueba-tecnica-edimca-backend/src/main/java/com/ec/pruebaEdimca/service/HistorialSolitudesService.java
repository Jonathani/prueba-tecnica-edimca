package com.ec.pruebaEdimca.service;

import java.util.List;

import com.ec.pruebaEdimca.dto.EstadoSolcitudesResponseDto;
import com.ec.pruebaEdimca.dto.HistorialDeSolicitudesResponseDto;

public interface HistorialSolitudesService {

	List<HistorialDeSolicitudesResponseDto> historialDeSolicitudes();
	
	List<EstadoSolcitudesResponseDto> estadoDeSolcitud();
}
