package com.ec.pruebaEdimca.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ec.pruebaEdimca.dao.CatalogoDao;
import com.ec.pruebaEdimca.dao.UsuarioDao;
import com.ec.pruebaEdimca.dto.EmployeeRequestDto;
import com.ec.pruebaEdimca.dto.InformacionUsuarioRequestDto;
import com.ec.pruebaEdimca.dto.InformacionUsuarioResponseDto;
import com.ec.pruebaEdimca.dto.LoginRequestDto;
import com.ec.pruebaEdimca.dto.SolicitudRequestDto;
import com.ec.pruebaEdimca.entity.Catalogo;
import com.ec.pruebaEdimca.entity.Usuario;
import com.ec.pruebaEdimca.service.UsuarioService;
import com.ec.pruebaEdimca.utilitarios.AES256;
import com.ec.pruebaEdimca.utilitarios.UtilitariosMensajes;

@Service
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	UsuarioDao usuarioDao;
	
	@Autowired
	CatalogoDao catalogoDao;
	
	@Override
	public void saveEmployee(EmployeeRequestDto employeeRequestDto) throws Exception {
		
		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
		
		if (usuarioDao.findUsuarioByName(employeeRequestDto.getNombre().trim()) == null) {
			
			Catalogo prioridad = catalogoDao.findCatalogoById(Long.parseLong(employeeRequestDto.getPrioridadId()));
			Catalogo tipoUsuario = catalogoDao.findCatalogoById(UtilitariosMensajes.EMPLOYEE_ID);
			Catalogo tipoSolicitud = catalogoDao.findCatalogoById(UtilitariosMensajes.SOLICITADA_ID);
			
			Usuario usuario = new Usuario();
			usuario.setNombre(employeeRequestDto.getNombre().trim());
			usuario.setDescripcion(employeeRequestDto.getDescripcion().trim());
			usuario.setPrioridad(prioridad);
			usuario.setTipoUsuario(tipoUsuario);
			usuario.setTipoSolicitud(tipoSolicitud);
			usuario.setCreatedAt(fechaActual);
			usuarioDao.save(usuario);
			
		}else {
			throw new Exception(UtilitariosMensajes.USUARIO_REGISTRADO);
		}
		
	}

	@Override
	public void updateEstadoSolicitud(long usuarioId, SolicitudRequestDto solicitudRequestDto) throws Exception {
		
		LocalDateTime now = LocalDateTime.now();
		Timestamp fechaActual = Timestamp.valueOf(now);
		
		Catalogo tipoSolicitud = catalogoDao.findCatalogoById(Long.parseLong(solicitudRequestDto.getEstadoSolicitudId()));
		
		Usuario usuario= usuarioDao.findUsuarioById(usuarioId);
		usuario.setUpdatedAt(fechaActual);
		usuario.setTipoSolicitud(tipoSolicitud);
		if (solicitudRequestDto.getUsuarioAsignadoId() != "0") {
			Usuario usuarioAsignado = usuarioDao.findUsuarioById(Long.parseLong(solicitudRequestDto.getUsuarioAsignadoId()));
			usuario.setUsuarioAsignado(usuarioAsignado);
		}
		usuarioDao.save(usuario);
		
	}

	@Override
	public boolean loginUsuario(LoginRequestDto loginRequestDto) throws Exception {		
		Usuario usuario = usuarioDao.findUsuarioByName(loginRequestDto.getUsername().trim());
		if (usuario != null && AES256.decrypt(usuario.getContrasenia()).equals(loginRequestDto.getPassword().trim()) && usuario.getTipoUsuario().getCatalogoId() == 6) {
			return true;
		}
		return false;
	}

	@Override
	public List<InformacionUsuarioResponseDto> informacionUsuarios(InformacionUsuarioRequestDto informacionUsuarioRequestDto) throws Exception {
		
		List<InformacionUsuarioResponseDto> usuarios = new ArrayList<>();
		
		//Viene por Analistas
		if (informacionUsuarioRequestDto.getNombreUsuario().equals("0")) {
			usuarioDao.findUsuarioByTipo(Long.parseLong(informacionUsuarioRequestDto.getTipoUsuarioId())).forEach(
				(e) -> {
					usuarios.add(
						new InformacionUsuarioResponseDto(String.valueOf(e.getUsuarioId()),e.getNombre(), e.getDescripcion())
					);
				}
			);
		}else if (informacionUsuarioRequestDto.getTipoUsuarioId().equals("0")) { 
		// Viene por informacion Usuario
			Usuario usu = usuarioDao.findUsuarioByName(informacionUsuarioRequestDto.getNombreUsuario());
			usuarios.add(
				new InformacionUsuarioResponseDto(String.valueOf(usu.getUsuarioId()),usu.getNombre(), usu.getDescripcion())
			);
		}		
		return usuarios;
	}
	
	
	

}
