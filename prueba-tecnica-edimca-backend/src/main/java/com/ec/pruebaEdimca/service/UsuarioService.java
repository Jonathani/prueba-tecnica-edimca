package com.ec.pruebaEdimca.service;

import java.util.List;

import com.ec.pruebaEdimca.dto.EmployeeRequestDto;
import com.ec.pruebaEdimca.dto.InformacionUsuarioRequestDto;
import com.ec.pruebaEdimca.dto.InformacionUsuarioResponseDto;
import com.ec.pruebaEdimca.dto.LoginRequestDto;
import com.ec.pruebaEdimca.dto.SolicitudRequestDto;

public interface UsuarioService {

	void saveEmployee (EmployeeRequestDto employeeRequestDto) throws Exception;
	
	void updateEstadoSolicitud (long usuarioId, SolicitudRequestDto solicitudRequestDto) throws Exception;
	
	boolean loginUsuario(LoginRequestDto loginRequestDto) throws Exception;
	
	List<InformacionUsuarioResponseDto> informacionUsuarios(InformacionUsuarioRequestDto informacionUsuarioRequestDto) throws Exception;
	
}
