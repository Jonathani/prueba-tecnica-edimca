package com.ec.pruebaEdimca.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ec.pruebaEdimca.dao.HistorialSolicitudesDao;
import com.ec.pruebaEdimca.dto.EstadoSolcitudesResponseDto;
import com.ec.pruebaEdimca.dto.HistorialDeSolicitudesResponseDto;
import com.ec.pruebaEdimca.service.HistorialSolitudesService;

@Service
public class HistorialSolitudesServiceImpl implements HistorialSolitudesService{
	
	@Autowired
	HistorialSolicitudesDao historialSolicitudesDao;

	@Override
	public List<HistorialDeSolicitudesResponseDto> historialDeSolicitudes() {
		
		List<HistorialDeSolicitudesResponseDto> historialSolicitudes = new ArrayList<>();
		
		historialSolicitudesDao.consultarHistorialSolicitudes().forEach(
			(e) -> {
				historialSolicitudes.add(
					new HistorialDeSolicitudesResponseDto(
						e[0] != null ? e[0].toString() : "", 
						e[1] != null ? e[1].toString() : "", 
						e[2] != null ? e[2].toString() : "",  
						e[3] != null ? e[3].toString() : "", 
						e[4] != null ? e[4].toString() : "", 
						e[5] != null ? e[5].toString() : "", 
						e[6] != null ? e[6].toString() : ""
					)
				);
			}
		);
		
		return historialSolicitudes;
	}

	@Override
	public List<EstadoSolcitudesResponseDto> estadoDeSolcitud() {
		
		List<EstadoSolcitudesResponseDto> estadosDeDeSolcitudes = new ArrayList<>();	
		historialSolicitudesDao.estadoDeSolcitudes().forEach(
			(e) -> {
				estadosDeDeSolcitudes.add(
					new EstadoSolcitudesResponseDto(
						e[0] != null ? e[0].toString() : "",
						e[1] != null ? e[1].toString() : ""
					)
				);				
			}
		);		
		return estadosDeDeSolcitudes;
	}

}
