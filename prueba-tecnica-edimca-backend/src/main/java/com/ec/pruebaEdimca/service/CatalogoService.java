package com.ec.pruebaEdimca.service;

import java.util.List;

import com.ec.pruebaEdimca.dto.CatalogoResponseDto;
import com.ec.pruebaEdimca.dto.ListarSolicitudesRequestDto;
import com.ec.pruebaEdimca.dto.ListarSolicitudesResponseDto;

public interface CatalogoService {
	
	List<CatalogoResponseDto> listarTipoPrioridad(long prioridadId);
	
	List<ListarSolicitudesResponseDto> listarSolicitudesPorId(ListarSolicitudesRequestDto listarSolicitudesRequestDto);

}
