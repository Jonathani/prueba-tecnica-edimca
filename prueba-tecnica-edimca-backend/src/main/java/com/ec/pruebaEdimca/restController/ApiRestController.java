package com.ec.pruebaEdimca.restController;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ec.pruebaEdimca.dto.EmployeeRequestDto;
import com.ec.pruebaEdimca.dto.EmployeeResponseDto;
import com.ec.pruebaEdimca.dto.InformacionUsuarioRequestDto;
import com.ec.pruebaEdimca.dto.ListarSolicitudesRequestDto;
import com.ec.pruebaEdimca.dto.LoginRequestDto;
import com.ec.pruebaEdimca.dto.MensajeResponseDto;
import com.ec.pruebaEdimca.dto.PrioridadesResponseDto;
import com.ec.pruebaEdimca.dto.RespuestaEstadoSolcitudesResponseDto;
import com.ec.pruebaEdimca.dto.RespuestaHistorialResponseDto;
import com.ec.pruebaEdimca.dto.RespuestaUsuariosResponseDto;
import com.ec.pruebaEdimca.dto.SolicitudRequestDto;
import com.ec.pruebaEdimca.dto.SolicitudesResponseDto;
import com.ec.pruebaEdimca.service.CatalogoService;
import com.ec.pruebaEdimca.service.HistorialSolitudesService;
import com.ec.pruebaEdimca.service.UsuarioService;
import com.ec.pruebaEdimca.utilitarios.UtilitariosMensajes;

@RestController
@RequestMapping("api")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT })
public class ApiRestController {

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	CatalogoService catalogoService;
	
	@Autowired
	HistorialSolitudesService HistorialSolitudesService;

	public static final String MENSAJE = "mensaje";

	@PostMapping(value = "saveEmployee", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> saveEmployee(@RequestBody EmployeeRequestDto employeeRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		EmployeeResponseDto employeeResponseDto;
		try {
			usuarioService.saveEmployee(employeeRequestDto);
			employeeResponseDto = new EmployeeResponseDto(true, UtilitariosMensajes.GUARDADO_EXITO);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<EmployeeResponseDto>(employeeResponseDto, HttpStatus.OK);
	}

	@PostMapping(value = "listarPrioridades", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> listarPrioridades() {

		Map<String, Object> responseMap = new HashMap<>();
		PrioridadesResponseDto prioridadesResponseDto;
		try {
			prioridadesResponseDto = new PrioridadesResponseDto(true,catalogoService.listarTipoPrioridad(UtilitariosMensajes.PRIORIDAD_ID));
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<PrioridadesResponseDto>(prioridadesResponseDto, HttpStatus.OK);
	}

	@PostMapping(value = "listarSolicitudes", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> listarSolicitudes(@RequestBody ListarSolicitudesRequestDto listarSolicitudesRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		SolicitudesResponseDto solicitudesResponseDto;
		try {
			solicitudesResponseDto = new SolicitudesResponseDto(true, "Correcto", catalogoService.listarSolicitudesPorId(listarSolicitudesRequestDto));
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<SolicitudesResponseDto>(solicitudesResponseDto, HttpStatus.OK);
	}
	
	@PutMapping(value = "actualizarEstado/{usuarioId}", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> editarCliente(@PathVariable("usuarioId") Long idCliente, @RequestBody SolicitudRequestDto solicitudRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		MensajeResponseDto mensajeResponseDto;
		try {
			usuarioService.updateEstadoSolicitud(idCliente, solicitudRequestDto);
			mensajeResponseDto = new MensajeResponseDto(true, UtilitariosMensajes.ACTUALIZADO_EXITO);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<MensajeResponseDto>(mensajeResponseDto, HttpStatus.OK);
	}
	
	@PostMapping(value = "login", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> listarSolicitudes(@RequestBody LoginRequestDto loginRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		MensajeResponseDto mensajeResponseDto;
		try {
			boolean estado = usuarioService.loginUsuario(loginRequestDto);
			String mensaje = estado ? UtilitariosMensajes.LOGIN_EXITOSO : UtilitariosMensajes.LOGIN_ERRONEO; 
			mensajeResponseDto = new MensajeResponseDto(estado, mensaje);
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<MensajeResponseDto>(mensajeResponseDto, HttpStatus.OK);
	}
	
	@PostMapping(value = "informacionUsuario", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> informacionUsuario(@RequestBody InformacionUsuarioRequestDto InformacionUsuarioRequestDto) {
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaUsuariosResponseDto respuestaUsuariosResponseDto;
		try {
			respuestaUsuariosResponseDto = new RespuestaUsuariosResponseDto(true, "Correcto", usuarioService.informacionUsuarios(InformacionUsuarioRequestDto));
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaUsuariosResponseDto>(respuestaUsuariosResponseDto, HttpStatus.OK);
	}
	
	@PostMapping(value = "historialSolicitudes", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> historialSolicitudes() {
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaHistorialResponseDto respuestaHistorialResponseDto = null;
		try {
			respuestaHistorialResponseDto = new RespuestaHistorialResponseDto(true, "Correcto", HistorialSolitudesService.historialDeSolicitudes());
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaHistorialResponseDto>(respuestaHistorialResponseDto, HttpStatus.OK);
	}
	
	@PostMapping(value = "estadosSolicitudes", produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> estadosSolicitudes() {
		Map<String, Object> responseMap = new HashMap<>();
		RespuestaEstadoSolcitudesResponseDto respuestaEstadoSolcitudesResponseDto = null;
		try {
			respuestaEstadoSolcitudesResponseDto = new RespuestaEstadoSolcitudesResponseDto(true, "Correcto", HistorialSolitudesService.estadoDeSolcitud());
		} catch (Exception e) {
			responseMap.put(MENSAJE, "Error");
			responseMap.put("mensaje", e.toString());
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<RespuestaEstadoSolcitudesResponseDto>(respuestaEstadoSolcitudesResponseDto, HttpStatus.OK);
	}

}
