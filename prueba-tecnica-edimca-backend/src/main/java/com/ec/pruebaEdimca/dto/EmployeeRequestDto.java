package com.ec.pruebaEdimca.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequestDto {
	
	private String nombre;
	
	private String descripcion;
	
	private String prioridadId;

}
