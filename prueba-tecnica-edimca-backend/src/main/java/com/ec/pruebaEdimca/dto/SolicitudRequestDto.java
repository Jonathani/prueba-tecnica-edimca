package com.ec.pruebaEdimca.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SolicitudRequestDto {
	
	private String estadoSolicitudId;
	
	private String usuarioAsignadoId;

}
