package com.ec.pruebaEdimca.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HistorialDeSolicitudesResponseDto {
	
	private String idHistorialSolicitud;
	
	private String idSolicitud;
	
	private String nombre;
	
	private String descripcion;
	
	private String estadoSolicitud;
	
	private String usuarioAsignado;
	
	private String fechaSolicitud;

}
