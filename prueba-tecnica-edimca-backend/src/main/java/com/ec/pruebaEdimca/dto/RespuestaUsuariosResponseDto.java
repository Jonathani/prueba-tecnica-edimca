package com.ec.pruebaEdimca.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RespuestaUsuariosResponseDto {
	
	private Boolean status;
	
	private String mensaje;
	
	private List<InformacionUsuarioResponseDto> usuarios;

}
