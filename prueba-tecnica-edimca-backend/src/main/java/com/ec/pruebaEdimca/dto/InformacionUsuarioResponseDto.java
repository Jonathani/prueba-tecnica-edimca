package com.ec.pruebaEdimca.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InformacionUsuarioResponseDto {
	
	private String usuarioId;
	
	private String nombreUsuario;
	
	private String descripcion;

}
