package com.ec.pruebaEdimca.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PrioridadesResponseDto {
	
	private boolean status;
	
	private List<CatalogoResponseDto> listadoDePrioridades;

}
