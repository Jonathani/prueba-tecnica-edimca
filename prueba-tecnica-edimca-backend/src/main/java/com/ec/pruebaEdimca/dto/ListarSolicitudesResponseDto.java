package com.ec.pruebaEdimca.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ListarSolicitudesResponseDto {
	
	String solicitudId;
	
	String nombre;
	
	String descripcion;
	
	String prioridad;

}
