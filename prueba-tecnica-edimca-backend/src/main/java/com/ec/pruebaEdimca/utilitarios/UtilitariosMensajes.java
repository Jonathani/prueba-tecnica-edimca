package com.ec.pruebaEdimca.utilitarios;

public class UtilitariosMensajes {
	
	public static final String GUARDADO_EXITO = "Informacion se ha guardado con exito";
	
	public static final String ACTUALIZADO_EXITO = "Informacion actualizada con exito";
	
	public static final String BORRADO_EXITO = "Informacion borrada con exito";
	
	public static final String USUARIO_REGISTRADO = "Estimado Usuario, este usuario ya ha sido registrado";
	
	public static final String USUARIO_NO_ENCONTRADO = "Estimado Usuario, este usuario no se encuentra registrado";
	
	public static final Long LONGITUD_CONTRASENIA = 50L;
	
	public static final String VALORES_MOVIMIENTO_EXITOSO= "Movimiento y/o cuenta registrado con exito";
	
	public static final String VALORES_MOVIMIENTO_INCORRECTO = "Estimado Usuario, el valor ingresados debe ser numerico entre positivo y negativo";
	
	public static final String SALDO_NO_DISPONIBLE= "Saldo no disponible";
	
	//Respuesta a Login
	public static final String LOGIN_EXITOSO = "El login fue existoso";
	public static final String LOGIN_ERRONEO = "Estimado Usuario, las credenciales ingresadas son incorrectas";
	
	//Tipo Empletado
	
	public static final int EMPLOYEE_ID = 7;
	
	public static final int MANAGER_ID = 6;
	
	// Tipo Prioridad
	public static final long PRIORIDAD_ID = 1;
	
	//Tipo solicitud
	
	public static final long SOLICITADA_ID = 9;
	public static final long APROBADA_ID = 10;
	public static final long ASIGNADA_ID = 11;

}
