package com.ec.pruebaEdimca.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_usuario", sequenceName = "public.seq_usuario", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "seq_usuario")
	@Column(name="usuario_id")
	private Long usuarioId;

	@Column(name="created_at")
	private Timestamp createdAt;
	
	private String contrasenia;

	private String descripcion;

	private String nombre;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	//bi-directional many-to-one association to Catalogo
	@ManyToOne
	@JoinColumn(name="prioridad_id")
	private Catalogo prioridad;

	//bi-directional many-to-one association to Catalogo
	@ManyToOne
	@JoinColumn(name="tipo_usuario_id")
	private Catalogo tipoUsuario;
	
	@ManyToOne
	@JoinColumn(name="tipo_solicitud_id")
	private Catalogo tipoSolicitud;
	
	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="usuario_asignado_id")
	private Usuario usuarioAsignado;
	
	//bi-directional many-to-one association to HistorialSolicitude
	@OneToMany(mappedBy="usuario")
	private List<HistorialSolicitudes> historialUsuario;

	//bi-directional many-to-one association to HistorialSolicitude
	@OneToMany(mappedBy="usuarioAsignado")
	private List<HistorialSolicitudes> historialUsuarioAsignado;

	
}