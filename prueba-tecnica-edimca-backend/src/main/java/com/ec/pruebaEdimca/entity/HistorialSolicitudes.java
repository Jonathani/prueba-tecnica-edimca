package com.ec.pruebaEdimca.entity;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;


/**
 * The persistent class for the historial_solicitudes database table.
 * 
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="historial_solicitudes")
@NamedQuery(name="HistorialSolicitudes.findAll", query="SELECT h FROM HistorialSolicitudes h")
public class HistorialSolicitudes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_historial_solicitudes", sequenceName = "public.seq_historial_solicitudes", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "seq_historial_solicitudes")
	@Column(name="historial_solicitudes_id")
	private Long historialSolicitudesId;

	@Column(name="created_at")
	private Timestamp createdAt;

	private String descripcion;

	private String nombre;

	//bi-directional many-to-one association to Catalogo
	@ManyToOne
	@JoinColumn(name="prioridad_id")
	private Catalogo prioridad;

	//bi-directional many-to-one association to Catalogo
	@ManyToOne
	@JoinColumn(name="tipo_solicitud_id")
	private Catalogo tipoSolicitud;

	//bi-directional many-to-one association to Catalogo
	@ManyToOne
	@JoinColumn(name="tipo_usuario_id")
	private Catalogo tipoUsuario;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="usuario_id")
	private Usuario usuario;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="usuario_asignado_id")
	private Usuario usuarioAsignado;	

}