package com.ec.pruebaEdimca.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@NamedQuery(name="Catalogo.findAll", query="SELECT c FROM Catalogo c")
public class Catalogo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "seq_catalogo", sequenceName = "public.seq_catalogo", allocationSize = 1)
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "seq_catalogo")
	@Column(name="catalogo_id")
	private Long catalogoId;

	private String descripcion;

	//bi-directional many-to-one association to Catalogo
	@ManyToOne
	@JoinColumn(name="catalogo_padre_id")
	private Catalogo catalogoPadre;

	//bi-directional many-to-one association to Catalogo
	@OneToMany(mappedBy="catalogoPadre")
	private List<Catalogo> catalogosPadre;

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="prioridad")
	private List<Usuario> usuariosPrioridad;

	//bi-directional many-to-one association to Usuario
	@OneToMany(mappedBy="tipoUsuario")
	private List<Usuario> usuariosTipo;
	
	@OneToMany(mappedBy="tipoSolicitud")
	private List<Usuario> usuariosSolicitud;
	
	//bi-directional many-to-one association to HistorialSolicitude
	@OneToMany(mappedBy="prioridad")
	private List<HistorialSolicitudes> historialPrioridad;

	//bi-directional many-to-one association to HistorialSolicitude
	@OneToMany(mappedBy="tipoSolicitud")
	private List<HistorialSolicitudes> historialTipoSolicitud;

	//bi-directional many-to-one association to HistorialSolicitude
	@OneToMany(mappedBy="tipoUsuario")
	private List<HistorialSolicitudes> historialTipoUsuario;
	
}