package com.ec.pruebaEdimca.sql;

public class ConsultasSQL {
	
	public static final String CONSULTA_SOLICITUDES_POR_ID = "select usu.usuario_id, usu.nombre, usu.descripcion, prior.descripcion as prioridad\n"
			+ "from public.usuario usu\n"
			+ "inner join public.catalogo tiposol\n"
			+ "on (usu.tipo_solicitud_id  = tiposol.catalogo_id)\n"
			+ "inner join public.catalogo prior\n"
			+ "on (prior.catalogo_id = usu.prioridad_id)\n"
			+ "where tiposol.catalogo_id = ?;";
	
	public static final String HISTORIAL_DE_SOLICITUDES = "select sol.historial_solicitudes_id, sol.usuario_id as id_solicitud, sol.nombre, sol.descripcion, tipo_sol.descripcion as tipo_solicitud, usu_asig.nombre as usuario_asignado, sol.created_at as fecha_solicitud\n"
			+ "from public.historial_solicitudes sol\n"
			+ "inner join public.catalogo tipo_sol\n"
			+ "on (sol.tipo_solicitud_id = tipo_sol.catalogo_id)\n"
			+ "left join public.usuario usu_asig\n"
			+ "on (usu_asig.usuario_id = sol.usuario_asignado_id)\n"
			+ "where sol.tipo_usuario_id = 7\n"
			+ "order by sol.created_at  desc;";
	
	public static final String CONTADOR_POR_ESTADOS_SOLICITUD = "select estado_sol.descripcion as estado, count(usu.usuario_id) as total\n"
			+ "from public.usuario usu \n"
			+ "inner join public.catalogo estado_sol\n"
			+ "on (usu.tipo_solicitud_id = estado_sol.catalogo_id)\n"
			+ "where tipo_usuario_id = 7\n"
			+ "group by usu.tipo_solicitud_id, estado_sol.descripcion;";

}
