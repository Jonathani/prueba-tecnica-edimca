package com.ec.pruebaEdimca.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ec.pruebaEdimca.entity.Catalogo;
import com.ec.pruebaEdimca.sql.ConsultasSQL;

@Repository
public interface CatalogoDao extends CrudRepository<Catalogo, Long>{

	@Transactional(readOnly = true)
	@Query(value = "SELECT catalogo FROM Catalogo catalogo WHERE catalogo.catalogoId = :catalogoId")
	Catalogo findCatalogoById(long catalogoId);	
	
	@Transactional(readOnly = true)
	@Query(value = " SELECT catalogo FROM Catalogo catalogo "
			+ " JOIN catalogo.catalogoPadre prioridad "
			+ " WHERE prioridad.catalogoId = :tipoPrioridadId "
			+ " ORDER BY catalogo.catalogoId ASC ")
	List<Catalogo> listarTipoPrioridad(long tipoPrioridadId);
	
	@Query(value = ConsultasSQL.CONSULTA_SOLICITUDES_POR_ID, nativeQuery = true)
	List<Object[]> consultarSolicitudesPorId(long tipoSolicitudId);
	
}
