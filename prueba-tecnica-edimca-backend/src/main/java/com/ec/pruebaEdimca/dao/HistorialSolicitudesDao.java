package com.ec.pruebaEdimca.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ec.pruebaEdimca.entity.HistorialSolicitudes;
import com.ec.pruebaEdimca.sql.ConsultasSQL;

@Repository
public interface HistorialSolicitudesDao extends CrudRepository<HistorialSolicitudes, Long>{

	@Query(value = ConsultasSQL.HISTORIAL_DE_SOLICITUDES, nativeQuery = true)
	List<Object[]> consultarHistorialSolicitudes();
	
	@Query(value = ConsultasSQL.CONTADOR_POR_ESTADOS_SOLICITUD, nativeQuery = true)
	List<Object[]> estadoDeSolcitudes();
}
