package com.ec.pruebaEdimca.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ec.pruebaEdimca.entity.Usuario;

@Repository
public interface UsuarioDao extends CrudRepository<Usuario, Long>{
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT usuario FROM Usuario usuario WHERE usuario.nombre = :nombre")
	Usuario findUsuarioByName(String nombre);
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT usuario FROM Usuario usuario WHERE usuario.usuarioId = :solicitudId")
	Usuario findUsuarioById(long solicitudId);
	
	@Transactional(readOnly = true)
	@Query(value = "SELECT usuario "
			+ "FROM Usuario usuario "
			+ "JOIN usuario.tipoUsuario tipoUsu "
			+ "WHERE tipoUsu.catalogoId = :tipoUsuarioId ")
	List<Usuario> findUsuarioByTipo(long tipoUsuarioId);

}
