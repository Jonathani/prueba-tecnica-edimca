#!/bin/bash
set -e
export PGPASSWORD=$POSTGRES_PASSWORD;
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
 CREATE USER $APP_DB_USER_LOCAL WITH PASSWORD '$APP_DB_PASS_LOCAL';
 ALTER ROLE $APP_DB_USER_LOCAL with SUPERUSER;
 CREATE DATABASE $APP_DB_NAME_LOCAL;
 GRANT ALL PRIVILEGES ON DATABASE $APP_DB_NAME_LOCAL TO $APP_DB_USER_LOCAL;
 GRANT ALL ON DATABASE $APP_DB_NAME_LOCAL TO $APP_DB_USER_LOCAL;
 ALTER DATABASE $APP_DB_NAME_LOCAL OWNER TO $APP_DB_USER_LOCAL;
 GRANT USAGE, CREATE ON SCHEMA PUBLIC TO $APP_DB_USER_LOCAL;
 GRANT ALL ON SCHEMA public TO $APP_DB_USER_LOCAL;
 GRANT USAGE ON schema public TO $APP_DB_USER_LOCAL;
 GRANT SELECT ON ALL TABLES IN SCHEMA public TO $APP_DB_USER_LOCAL;
 GRANT CREATE ON SCHEMA public TO $APP_DB_USER_LOCAL;
 \connect $APP_DB_NAME_LOCAL $APP_DB_USER_LOCAL
 BEGIN;

    --Catalogo
    CREATE TABLE public.catalogo
    (
    catalogo_id int8 not null
    , descripcion text
    , catalogo_padre_id int8
    , CONSTRAINT catalogo_pkey PRIMARY KEY (catalogo_id)
    );
    ALTER TABLE public.catalogo ADD CONSTRAINT fk_catalogo FOREIGN KEY (catalogo_padre_id) REFERENCES public.catalogo(catalogo_id);

    CREATE SEQUENCE public.seq_catalogo
            INCREMENT BY 1
            MINVALUE 1
            MAXVALUE 9223372036854775807
            START 1
            CACHE 1
            NO CYCLE;
        
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(1, 'Tipo Prioridad', null);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(2, 'High', 1);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(3, 'Normal', 1);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(4, 'Low', 1);

    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(5, 'Tipo Usuario', null);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(6, 'Manager', 5);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(7, 'Employee', 5);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(12, 'Analista', 5);

    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(8, 'Tipo Solicitud', null);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(9, 'Pending', 8);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(10, 'Approved', 8);
    INSERT INTO public.catalogo
    (catalogo_id, descripcion, catalogo_padre_id)
    VALUES(11, 'Assigned', 8);
            
    --Usuario
    CREATE TABLE public.usuario
    (
    usuario_id int8 not null
    , nombre text
    , contrasenia text
    , descripcion text
    , prioridad_id int8
    , tipo_usuario_id int8
    , tipo_solicitud_id int8
    , usuario_asignado_id int8
    , created_at timestamp NOT null
    , updated_at timestamp
    , CONSTRAINT usuario_pkey PRIMARY KEY (usuario_id)
    );
    ALTER TABLE public.usuario ADD CONSTRAINT fk_prioridad FOREIGN KEY (prioridad_id) REFERENCES public.catalogo(catalogo_id);
    ALTER TABLE public.usuario ADD CONSTRAINT fk_tipo_usuario FOREIGN KEY (tipo_usuario_id) REFERENCES public.catalogo(catalogo_id);
    ALTER TABLE public.usuario ADD CONSTRAINT fk_tipo_solicitud FOREIGN KEY (tipo_solicitud_id) REFERENCES public.catalogo(catalogo_id);
    ALTER TABLE public.usuario ADD CONSTRAINT fk_usuario_asignado FOREIGN KEY (usuario_asignado_id) REFERENCES public.usuario(usuario_id);

    CREATE SEQUENCE public.seq_usuario
            INCREMENT BY 1
            MINVALUE 1
            MAXVALUE 9223372036854775807
            START 1
            CACHE 1
            NO CYCLE;

    INSERT INTO public.usuario
    (usuario_id, nombre, contrasenia, descripcion, prioridad_id, tipo_usuario_id, tipo_solicitud_id, usuario_asignado_id, created_at, updated_at)
    VALUES(1, 'Administrador', '+5WfpeC5M+/9aD5SlGNIz8KgffndmwYN0sqTAKFDtDw=', 'Usuario Administrador', null, 6, null, null, now(), null);
    INSERT INTO public.usuario
    (usuario_id, nombre, contrasenia, descripcion, prioridad_id, tipo_usuario_id, tipo_solicitud_id, usuario_asignado_id, created_at, updated_at)
    VALUES(2, 'Analista 1', null, 'Usuario Analista 1', null, 12, null, null, now(), null);
    INSERT INTO public.usuario
    (usuario_id, nombre, contrasenia, descripcion, prioridad_id, tipo_usuario_id, tipo_solicitud_id, usuario_asignado_id, created_at, updated_at)
    VALUES(3, 'Analista 2', null, 'Usuario Analista 2', null, 12, null, null, now(), null);
    INSERT INTO public.usuario
    (usuario_id, nombre, contrasenia, descripcion, prioridad_id, tipo_usuario_id, tipo_solicitud_id, usuario_asignado_id, created_at, updated_at)
    VALUES(4, 'Analista 3', null, 'Usuario Analista 3', null, 12, null, null, now(), null);

    ALTER SEQUENCE public.seq_usuario RESTART WITH 5;

    CREATE TABLE public.historial_solicitudes
    (
    historial_solicitudes_id int8 NOT NULL
    , usuario_id int8
    , nombre text
    , descripcion text
    , prioridad_id int8
    , tipo_usuario_id int8
    , tipo_solicitud_id int8
    , usuario_asignado_id int8
    , created_at timestamp NOT null
    , CONSTRAINT historial_solicitudes_pkey PRIMARY KEY (historial_solicitudes_id)
    );
    ALTER TABLE public.historial_solicitudes ADD CONSTRAINT fk_historial_solicitudes_usuario FOREIGN KEY (usuario_id) REFERENCES public.usuario(usuario_id);
    ALTER TABLE public.historial_solicitudes ADD CONSTRAINT fk_historial_solicitudes_prioridad FOREIGN KEY (prioridad_id) REFERENCES public.catalogo(catalogo_id);
    ALTER TABLE public.historial_solicitudes ADD CONSTRAINT fk_historial_solicitudes_tipo_usuario FOREIGN KEY (tipo_usuario_id) REFERENCES public.catalogo(catalogo_id);
    ALTER TABLE public.historial_solicitudes ADD CONSTRAINT fk_historial_solicitudes_tipo_solicitud FOREIGN KEY (tipo_solicitud_id) REFERENCES public.catalogo(catalogo_id);
    ALTER TABLE public.historial_solicitudes ADD CONSTRAINT fk_historial_solicitudes_usuario_asignado FOREIGN KEY (usuario_asignado_id) REFERENCES public.usuario(usuario_id);

    CREATE SEQUENCE public.seq_historial_solicitudes
            INCREMENT BY 1
            MINVALUE 1
            MAXVALUE 9223372036854775807
            START 1
            CACHE 1
            NO CYCLE;
        
    CREATE OR REPLACE FUNCTION public."trigger_historial_solicitudes"()
    RETURNS trigger AS
    \$BODY$
    BEGIN
    ----------------------------------------------------------------------------------------------------------------------
    IF (TG_OP = 'DELETE') THEN
    RAISE EXCEPTION 'NO SE PERMITEN BORRADOS FISICOS';
    END IF ;
    ----------------------------------------------------------------------------------------------------------------------

    IF (TG_OP = 'INSERT') THEN

    INSERT INTO public.historial_solicitudes
    (historial_solicitudes_id, usuario_id, nombre, descripcion, prioridad_id, tipo_usuario_id, tipo_solicitud_id, usuario_asignado_id, created_at)
    VALUES((SELECT nextval('public.seq_historial_solicitudes')), new.usuario_id, new.nombre, new.descripcion, new.prioridad_id, new.tipo_usuario_id, new.tipo_solicitud_id, new.usuario_asignado_id, now());
    return new ;
    end IF ;
    ----------------------------------------------------------------------------------------------------------------------

    IF (TG_OP = 'UPDATE') THEN

    INSERT INTO public.historial_solicitudes
    (historial_solicitudes_id, usuario_id, nombre, descripcion, prioridad_id, tipo_usuario_id, tipo_solicitud_id, usuario_asignado_id, created_at)
    VALUES((SELECT nextval('public.seq_historial_solicitudes')), new.usuario_id, new.nombre, new.descripcion, new.prioridad_id, new.tipo_usuario_id, new.tipo_solicitud_id, new.usuario_asignado_id, now());
    return new ;
    END IF ;

    ----------------------------------------------------------------------------------------------------------------------
    END;
    \$BODY$
    LANGUAGE plpgsql VOLATILE;


    CREATE TRIGGER triggerHistorialSolicitudes
    AFTER INSERT OR UPDATE OR DELETE
    ON public.usuario
    FOR EACH ROW
    EXECUTE PROCEDURE public."trigger_historial_solicitudes"();

COMMIT;

EOSQL
